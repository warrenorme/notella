var express = require("express")
var app = express()
var session = require('express-session')
var mysql = require('mysql')
var html = require("html")
var md5 = require("md5")
const nodemailer = require("nodemailer")
var transporter = nodemailer.createTransport({
    service:"gmail",
    auth:{
        user:"warrentest72@gmail.com",
        pass:"0Q99a8a8MrgP@7$uXkUa"

    }
})
var connection = mysql.createConnection({
    host: "localhost",
    user : "root",
    password: "root",
    database : "my_db"
})


app.use(session({
    secret: '54132',
    resave: false,
    saveUninitialized: true,
    name : "Test",
    userid : "",
    loggedin: false,
    token : "",
    email : ""
})) 
app.set("view engine", "ejs")
app.use('/public', express.static('public'));
var bodyparser = require("body-parser")
const { json } = require("body-parser")
var urlencodedParser = bodyparser.urlencoded({ extended: false })
const port = 3000
app.get("/",(req,res)=>{ // When the user loads the url this section of code runs
    if (req.session.loggedin == true){ //Checks if the user was previously logged in
        res.render("main.ejs")
    }
    else{ res.render("index.ejs",{errormessage: "" })}
})
app.get("/register",(req,res)=>{// When a user goes to /register this section of code is run
    res.render("register.ejs",{message:""})

})
app.post("/register", urlencodedParser, (req,res)=>{ // When a post request is made to /register this section of code is run
    connection.query("SELECT email FROM my_db.users WHERE email='" + connection.escapeId(req.body.email) + "' OR user='" + connection.escapeId(req.body.user) + "'", function(error,results){
        if (results.length === 0){
            connection.connect(function(error){
                console.log("connected")
                var sql = "INSERT INTO users (user,password,email) VALUES('"+  connection.escapeId(req.body.user) + "', '" + md5(req.body.password) + "','" + connection.escapeId(req.body.email) + "')" // The use of escapeId is to stop accidently sql injections 

                connection.query(sql,function(error,results){
                    if (error) {throw error}
                })
            })
            res.render("register.ejs",{message:"The account has been registered succesfully"})
        }
        else(res.render("register.ejs",{message:"Username or Email is already in use"}))

    })
})
app.post("/login", urlencodedParser,(req,res)=>{
    var passData
    console.log(req.body)
    try { //Try a d catch to stop any errors from breaking the program completly 
        var sql = "SELECT * FROM my_db.users WHERE user='"+ connection.escapeId(req.body.user) +"'" //Santises the input from the user 
    connection.query(sql ,function(error, results) { // Gets the password stored from database 

       if (results != undefined){
           if (results != 0){
            var dataFromQuery = JSON.parse(JSON.stringify(results[0]),4) //Makes the request to the database a json object so it can be used in
            var userPassword = dataFromQuery.password
            if (userPassword === md5(req.body.pass)){ // Compares the hashed users password with the value they have entered
                req.session.loggedin = true 
                req.session.userid = dataFromQuery.idusers // Sets the user id in the cookie to the current user 
                res.render("main.ejs")
            }
            else{
                res.render("index.ejs",{errormessage:"Invaild password"})
            }
         }
         else{res.render("index.ejs",{errormessage:"Invaild username"})}
        }
        else{console.log(results)}
    });
    }
    catch(error){
        res.render("index.ejs",{errormessage:"Invaild username or password"})
    }
})
app.get("/api/getNotes",(req,res)=>{
    connection.query("SELECT * FROM my_db.notes WHERE boardid='" + req.query.boardid + "'",function(error,results){
        if (error){console.log(error)}
        else {
            var dataFromQuery = JSON.parse(JSON.stringify(results),4)
            console.log(dataFromQuery)
            res.json(dataFromQuery)
        }

    })
})

app.get("/forgottenpassword",(req,res)=>{
    res.render("forgottenpassword.ejs")
})
app.post("/resetpassword",urlencodedParser,(req,res)=>{
    connection.query("SELECT email FROM my_db.users WHERE email='" + connection.escapeId(req.body.email) + "'",function(error,results){
        if (error){console.log(error)}
       else{ var dataFromQuery = JSON.parse(JSON.stringify(results),4).map(l=>l.email)
        console.log(dataFromQuery)
        if (dataFromQuery.length === 1){
            req.session.token = Math.floor(Math.random()*99999)
            var mail = {
                from:"warrentest72@gmail.com",
                to:req.body.email,
                subject:"Password Reset",
                text:"http://localhost:3000/changepassword?token=" + req.session.token + "&email=" + dataFromQuery 
            }
            transporter.sendMail(mail,function(error,info){
                if (error) {
                    console.log(error)
                }
                else{console.log("Success")}
            })
        }
       }
       res.redirect("/")
    })



})
app.get("/changepassword",(req,res)=>{
    if (req.query.token == req.session.token){
        req.session.email = req.query.email
        res.render("changepassword.ejs")
    }
    else{
        res.render("rejected.ejs")
    }
})
app.post("/passwordchange",urlencodedParser,(req,res)=>{
    connection.connect(function(error){
        console.log(req.session.email)
        var sql = "UPDATE users SET password='" + md5(req.body.password) +"' WHERE email='" + connection.escapeId(req.session.email) + "'"
        connection.query(sql,function(error,results){
            if (error){
                console.log(error)
            }
        })
    })
    res.redirect("/")
})
app.get("/api/userBoards",(req,res)=>{
    connection.query("SELECT boardid FROM my_db.perms WHERE userid='" + req.session.userid + "'", function (error,results){
        if (error){console.log(error)}
        else {
            var dataFromQuery = JSON.parse(JSON.stringify(results),4).map(l=>l.boardid)
            console.log(dataFromQuery)
            res.json(dataFromQuery)
        }
    })
})
app.post("/api/addCard",urlencodedParser,(req,res)=>{
    connection.connect(function(error){
        var sql = "INSERT INTO notes (title,notes,userid,boardid) VALUES('" + req.body.title +"', '" + req.body.description + "', '" + req.session.userid + "', '" + req.body.boardid + "')" 
        connection.query(sql,function(error,results){
            if (error){
                console.log(error)
            }
        })

    })
    res.redirect("/")
})
app.post("/api/deleteCard",urlencodedParser,(req,res)=>{
    var idtodelete = JSON.parse(JSON.stringify(req.body));
    connection.connect(function(error){
        var sql = "DELETE FROM notes WHERE notesid='" + idtodelete.id + "'"
        connection.query(sql,function(error,results){
            if (error){ console.log(error)}
        })
    })
})
app.post("/api/addBoard",(req,res)=>{
    connection.connect(function(error){
        var sql = "SELECT boardid FROM perms"
        connection.query(sql,function(error,results){
            if (error){ console.log(error)}
            else {
                console.log("Succes 1")
                var nextboard = JSON.parse(JSON.stringify(results),4).map(l=>l.boardid)
                var nextboardfinal = Math.max(...nextboard) + 1
                console.log(nextboardfinal)
                var sql = "INSERT INTO perms (boardid,userid) VALUES('" + nextboardfinal + "', '" + req.session.userid + "')"
                connection.query(sql,function(error,results){
                    if (error){ console.log(error)}
                    else{
                        res.redirect("/")
                    }
                })

            }
        })
    })
})
app.post("/api/updateCard",urlencodedParser,(req,res)=>{
    connection.connect(function(error){
        var sql = "UPDATE notes SET title='" + req.body.updatetitle +"', notes='" + req.body.updatedescription +"'"  +  "WHERE notesid='" + req.body.noteid + "'" 
        connection.query(sql,function(error,results){
            if (error){ console.log(error)}
        })
        res.redirect("/")

    })
})
app.get("/logout",(req,res)=>{
    req.session.destroy()
    res.redirect("/")
})
app.post("/api/shareUser",urlencodedParser,(req,res)=>{
    // TODO that wil never done
    var sql = "SELECT idusers FROM users WHERE user='" + connection.escapeId(req.body.share) + "'"
    connection.query(sql,function(error,results){
        if (error){console.log(error)}
        else{
            console.log(results)
            console.log(results.length)
            var result = results.map(l => l.idusers)
            if (results.length != 0){
                var sql = "INSERT INTO perms (boardid,userid) VALUES('" + req.body.boardid + "', '" + result[0] + "')"
                connection.query(sql,function(error,results){
                    if (error){ console.log(error)}
                    else(console.log("done"))
                })
            }
            res.redirect("/")
        }
    })

})
app.listen(port)
